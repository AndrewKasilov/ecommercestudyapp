package com.action.ecommerceapp.data.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {

    private final static long serialVersionUID = -8352986974408403558L;
    private static final String BASE_IMAGE_URL = "http://akcijske-cijene.xyz/img/items/thumbs/";

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("quantity")
    @Expose
    private long quantity;
    @SerializedName("qty_unit_id")
    @Expose
    private long qtyUnitId;
    @SerializedName("category_id")
    @Expose
    private long categoryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return BASE_IMAGE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getQtyUnitId() {
        return qtyUnitId;
    }

    public void setQtyUnitId(long qtyUnitId) {
        this.qtyUnitId = qtyUnitId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

}
