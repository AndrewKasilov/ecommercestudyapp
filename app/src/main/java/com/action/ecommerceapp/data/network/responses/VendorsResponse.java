package com.action.ecommerceapp.data.network.responses;

import com.action.ecommerceapp.data.network.models.Vendor;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VendorsResponse implements Serializable {

    private final static long serialVersionUID = -3174205164276087694L;
    @SerializedName("vendors")
    @Expose
    private List<Vendor> vendors = null;

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }
}
