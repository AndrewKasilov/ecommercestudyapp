package com.action.ecommerceapp.data.network.responses;

import com.action.ecommerceapp.data.network.models.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductsResponse implements Serializable {

    private final static long serialVersionUID = -8049825350282931834L;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
