package com.action.ecommerceapp.data.network;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * {@link RetrofitNetManager} is used to create request to server and handle responses
 */
public class RetrofitNetManager {

    private static final String BASE_URL = "http://akcijske-cijene.xyz/api/public/v1/";
    private static RetrofitNetManager instance;
    private Retrofit retrofit;

    private RetrofitNetManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * We make this class to be a singleton
     *
     * @return an instance of {@link RetrofitNetManager}
     */
    public static RetrofitNetManager getInstance() {
        if (instance == null) {
            instance = new RetrofitNetManager();
        }
        return instance;
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return httpClient.build();
    }

    public ServerApi getServerApi() {
        return retrofit.create(ServerApi.class);
    }

}
