package com.action.ecommerceapp.domain.interactors;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * {@link UseCase} is a base abstract. UseCases are used to orchestrate
 * the flow of data to and from the entities. Also known as Interactors
 *
 * @param <T>      type of the return data
 * @param <Params> parameters, that could be used in interactors
 */
public abstract class UseCase<T, Params> {

    private final CompositeDisposable compositeDisposable;

    protected UseCase() {
        this.compositeDisposable = new CompositeDisposable();
    }

    /**
     * Subscribes an observer to an observable on the Schedulers.io() thread and observes it
     * on the AndroidSchedulers.mainThread() thread
     *
     * @param observer a {@link DisposableObserver} that subscribes on the observable
     * @param params   additional parameters
     */
    public void execute(@NonNull final DisposableObserver<T> observer, @Nullable final Params params) {
        final Observable<T> observable = buildObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        addDisposable(observable.subscribeWith(observer));
    }

    protected abstract Observable<T> buildObservable(@Nullable final Params params);

    /**
     * Disposes all observables
     */
    public void dispose() {
        this.compositeDisposable.dispose();
    }

    private void addDisposable(@NonNull final DisposableObserver<T> observer) {
        this.compositeDisposable.add(observer);
    }
}