package com.action.ecommerceapp.presentation.view.interfaces;

/**
 * {@link IDataLoader} an interface to interact between presenter and view
 *
 * @param <T> data
 */
public interface IDataLoader<T> {
    /**
     * A method that is called when a presenter has loaded data
     *
     * @param data - data
     */
    void onDataLoaded(T data);

    /**
     * Used to hide a progressbar
     */
    void hideLoading();

    /**
     * Used to show a progressbar
     */
    void showLoading();
}
