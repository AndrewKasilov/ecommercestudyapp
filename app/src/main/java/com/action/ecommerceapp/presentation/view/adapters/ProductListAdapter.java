package com.action.ecommerceapp.presentation.view.adapters;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.data.network.models.Category;
import com.action.ecommerceapp.data.network.models.Child;
import com.action.ecommerceapp.data.network.models.Product;
import com.action.ecommerceapp.data.network.models.Vendor;
import com.action.ecommerceapp.presentation.model.FilterModel;
import com.action.ecommerceapp.presentation.utils.TextFormatUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ItemViewHolder> {


    private static final String TAG = ProductListAdapter.class.getSimpleName();
    private final ItemViewHolder.OnItemClickListener onItemClickListener;
    private List<Product> data;

    public ProductListAdapter(@NonNull final ItemViewHolder.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ItemViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final Product product = data.get(position);

        final String productDate = TextFormatUtils.getFormattedProductDate(holder.itemView.getContext(), product);
        final SpannableString productPrice = TextFormatUtils.getFormattedProductPrice(holder.itemView.getContext(), product);

        holder.tvProductName.setText(product.getItem().getName());
        holder.tvProductVendor.setText(product.getVendor().getName());
        holder.tvProductDate.setText(productDate);
        holder.tvProductPrice.setText(productPrice);
        holder.tvProductDiscount.setText(product.getDiscountPercentage());

        Glide.with(holder.itemView.getContext())
                .load(product.getItem().getImage())
                .into(holder.ivProductImage);
    }


    @Override
    public int getItemCount() {
        return this.data == null ? 0 : this.data.size();
    }

    public void setData(@NonNull final List<Product> productList, @Nullable final FilterModel filters) {
        final List<Product> products = new ArrayList<>(productList);
        if (filters != null) {
            filterProductList(products, filters);
        }

        if (this.data == null) {
            this.data = new ArrayList<>(products);
            notifyItemRangeInserted(0, data.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return data.size();
                }

                @Override
                public int getNewListSize() {
                    return products.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return data.get(oldItemPosition).getId() == products.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return data.get(oldItemPosition).getItem() == products.get(newItemPosition).getItem()
                            && data.get(oldItemPosition).getItemId() == products.get(newItemPosition).getItemId()
                            && Objects.equals(data.get(oldItemPosition).getItem().getDescription(), products.get(newItemPosition).getItem().getDescription());
                }
            }, false);
            this.data = new ArrayList<>(products);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    private void filterProductList(List<Product> productList, @NonNull final FilterModel filters) {
        ListIterator<Product> productListIterator = productList.listIterator();
        while (productListIterator.hasNext()) {
            final Product product = productListIterator.next();
            if (isInCategoryBlackList(product, filters.getCategoryList())
                    | isInVendorBlackList(product, filters.getVendorList())
                    | !isPriceFits(filters, product)) {
                productListIterator.remove();
            }
        }
    }

    private boolean isPriceFits(@NonNull FilterModel filters, Product product) {
        return TextUtils.isEmpty(filters.getMaxPrice()) || Double.parseDouble(product.getNewPrice()) < Double.parseDouble(filters.getMaxPrice());
    }

    /**
     * Check if product not in the vendor filter list
     */
    private boolean isInVendorBlackList(@NonNull final Product product, @NonNull final List<Vendor> vendorList) {
        for (Vendor vendor : vendorList) {
            if (!vendor.isChecked() && product.getVendorId() == vendor.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if product not in the categories filter list
     */
    private boolean isInCategoryBlackList(@NonNull final Product product, @NonNull final List<Category> categoryList) {
        for (Category category : categoryList) {
            for (Child child : category.getChildren()) {
                if (!child.isChecked() && product.getItem().getCategoryId() == child.getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private final TextView tvProductName;
        private final TextView tvProductPrice;
        private final TextView tvProductDate;
        private final TextView tvProductVendor;
        private final TextView tvProductDiscount;
        private final ImageView ivProductImage;

        public ItemViewHolder(View itemView, final OnItemClickListener onItemClickListener) {
            super(itemView);

            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductPrice = itemView.findViewById(R.id.tvProductPrice);
            tvProductDate = itemView.findViewById(R.id.tvProductDate);
            tvProductVendor = itemView.findViewById(R.id.tvProductVendor);
            tvProductDiscount = itemView.findViewById(R.id.tvProductDiscount);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClicked(getAdapterPosition());
                }
            });
        }


        public interface OnItemClickListener {
            void onItemClicked(int position);
        }
    }
}
