package com.action.ecommerceapp.presentation.presenter;


import com.action.ecommerceapp.domain.interactors.FiltersUseCase;
import com.action.ecommerceapp.presentation.model.FilterModel;
import com.action.ecommerceapp.presentation.view.interfaces.IDataLoader;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

public class ProductsFilterPresenterImpl implements ProductsFilterPresenter {

    private final IDataLoader<FilterModel> view;
    private final FiltersUseCase useCase;
    private FilterModel filterModel;

    public ProductsFilterPresenterImpl(IDataLoader<FilterModel> filterModelIDataLoader) {
        this.view = filterModelIDataLoader;
        this.useCase = new FiltersUseCase();
    }

    @Override
    public void onStop() {
        this.useCase.dispose();
    }

    @Override
    public void loadData() {
        this.view.showLoading();
        if (filterModel == null) {
            this.useCase.execute(new FiltersObserver(), null);
        }
    }

    @Override
    public void onCategoryClicked(final int groupPosition, final int childPosition,
                                  final boolean isChecked) {
        if (filterModel != null && filterModel.getCategoryList() != null
                && filterModel.getCategoryList().get(groupPosition).getChildren() != null) {
            filterModel.getCategoryList().get(groupPosition).getChildren().get(childPosition).setChecked(isChecked);
        }
    }

    @Override
    public void onVendorChecked(final int position, final boolean isChecked) {
        filterModel.getVendorList().get(position).setChecked(isChecked);
    }

    @Override
    public FilterModel getFilters() {
        return filterModel;
    }

    @Override
    public void setFilters(@NonNull final FilterModel filters) {
        this.filterModel = filters;
    }


    private class FiltersObserver extends DisposableObserver<FilterModel> {
        @Override
        public void onNext(@NonNull final FilterModel filterModel) {
            view.onDataLoaded(filterModel);
            ProductsFilterPresenterImpl.this.filterModel = filterModel;
        }

        @Override
        public void onError(@NonNull final Throwable e) {
            view.hideLoading();
        }

        @Override
        public void onComplete() {
            view.hideLoading();

        }
    }
}
