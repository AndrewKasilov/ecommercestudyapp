package com.action.ecommerceapp.presentation.view.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.presentation.model.FilterModel;
import com.action.ecommerceapp.presentation.view.fragments.ProductListFragment;
import com.action.ecommerceapp.presentation.view.fragments.ProductsFilterFragment;
import com.action.ecommerceapp.presentation.view.interfaces.IFilterCallback;

public class ProductsListActivity extends BaseActivity implements IFilterCallback {

    private ProductListFragment productListFragment;
    private FilterModel filterModel;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        productListFragment = new ProductListFragment();
        this.addFragment(R.id.fragmentContainer, productListFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull final Menu menu) {
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menuFilter:
                openFilterScreen();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openFilterScreen() {
        Bundle bundle = null;
        if (filterModel != null) {
            bundle = new Bundle();
            bundle.putSerializable(ProductsFilterFragment.KEY_FILTERS_MODEL, filterModel);
        }
        final ProductsFilterFragment productsFilterFragment = new ProductsFilterFragment();
        productsFilterFragment.setArguments(bundle);
        this.replaceFragment(R.id.fragmentContainer, productsFilterFragment);
    }


    @Override
    public void onFilterSelected(@Nullable final FilterModel filterModel) {
        this.filterModel = filterModel;
        if (this.productListFragment != null) {
            productListFragment.setFilters(filterModel);
        }
    }
}
