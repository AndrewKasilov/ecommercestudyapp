package com.action.ecommerceapp.presentation.view.interfaces;


import android.support.annotation.Nullable;

import com.action.ecommerceapp.presentation.model.FilterModel;

public interface IFilterCallback {
    void onFilterSelected(@Nullable FilterModel filterModel);
}
