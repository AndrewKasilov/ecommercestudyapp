package com.action.ecommerceapp.presentation.view.activities;


import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * {@link BaseActivity} orchestrates all activities that extend it
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final String BACK_STACK_NAME = "BACK_STACK_NAME";

    /**
     * Method for adding a fragment to the container
     *
     * @param fragmentContainer container view id
     * @param fragment          fragment to add
     */
    public void addFragment(final int fragmentContainer, @NonNull final Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(fragmentContainer, fragment)
                .addToBackStack(BACK_STACK_NAME)
                .commit();
    }

    /**
     * Method for replacing a fragment in the container
     * *
     *
     * @param fragmentContainer container view id
     * @param fragment          fragment to replace with
     */
    public void replaceFragment(final int fragmentContainer, @NonNull final Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(fragmentContainer, fragment)
                .addToBackStack(BACK_STACK_NAME)
                .commit();
    }
}
