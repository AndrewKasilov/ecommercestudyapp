package com.action.ecommerceapp.presentation.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.action.ecommerceapp.R;
import com.action.ecommerceapp.data.network.models.Product;
import com.action.ecommerceapp.presentation.model.FilterModel;
import com.action.ecommerceapp.presentation.presenter.Presenter;
import com.action.ecommerceapp.presentation.presenter.ProductListPresenterImpl;
import com.action.ecommerceapp.presentation.view.adapters.ProductListAdapter;
import com.action.ecommerceapp.presentation.view.interfaces.IDataLoader;

import java.util.List;

public class ProductListFragment extends BaseFragment implements IDataLoader<List<Product>>,
        ProductListAdapter.ItemViewHolder.OnItemClickListener {

    private static final String TAG = ProductListFragment.class.getSimpleName();
    private static final int SPAN_COUNT = 2;
    private ProductListAdapter productListAdapter;
    private Presenter presenter;
    private ProgressBar progressBar;
    private FilterModel filters;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.presenter = new ProductListPresenterImpl(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {

        this.progressBar = view.findViewById(R.id.progressBar);
        // initialize recyclerView (list)
        final RecyclerView recycleListView = view.findViewById(R.id.rvProductList);
        this.productListAdapter = new ProductListAdapter(this);

        recycleListView.setHasFixedSize(false);
        recycleListView.setAdapter(productListAdapter);
        recycleListView.setLayoutManager(new GridLayoutManager(getActivity(), SPAN_COUNT));

        this.presenter.loadData();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.presenter.onStop();
    }

    @Override
    public void onDataLoaded(List<Product> data) {
        this.productListAdapter.setData(data, filters);
        Log.d(TAG, "onDataLoaded: ");
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClicked(int position) {

    }

    public void setFilters(@Nullable final FilterModel filterModel) {
        this.filters = filterModel;
    }
}
